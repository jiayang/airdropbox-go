﻿Vue.component("password-input", {
    props: {
        optional: true
    },
    data() {
        return {
            passwordMatch: true
        }
    },
    methods: {
        togglePassword(event: Event) {
            const input: HTMLInputElement = this.$refs.passwordInput;

            if (input.type === "password") {
                input.type = "text";

                const icon: HTMLElement = this.$refs.togglePasswordIcon;
                icon.innerHTML = "visibility_off";
            } else {
                input.type = "password";

                const icon: HTMLElement = this.$refs.togglePasswordIcon;
                icon.innerHTML = "visibility";
            }
        }
    },
    template: `
        <div>
            <div class="input-wrapper">
                <input v-if="!optional" ref="passwordInput"
                       name="password" type="password" placeholder="Password" required />
                <input v-else ref="passwordInput"
                       name="password" type="password" placeholder="Password (optional)" />
                       
                <i ref="togglePasswordIcon"
                   v-on:click="togglePassword"
                   class="material-icons md-24 md-light">visibility</i>
            </div>
    
            <transition name="slide-vert">
                <div v-if="!passwordMatch">
                    <span class="caption note caution">Incorrect password</span>
                </div>
            </transition>
        </div>
    `
});

Vue.component("error-message", {
    data() {
        return {
            isError: false
        }
    },
    methods: {
        showHideError() {
            this.isError = true;
            setTimeout(() => {
                this.isError = false;
            }, 3000);
        }
    },
    template: `
        <transition name="slide-vert">
            <div v-if="isError"
                 class="error-banner">
                <span class="caption note warning">Something broke!</span>
            </div>
        </transition>
    `
});

const app = new Vue({
    delimiters: ["${", "}"],
    el: "#app",
    data: {
        filename: "Choose a file or drop it here.",
        fileDropped: false,
        uploading: false,
        uploaded: false,
        downloaded: false,
        totalSize: 0,
        completedPercentage: 0,
        completedSize: 0,
        instantSpeed: 0,
        downloadUrl: ""
    },
    computed: {
        humanReadableTotalSize() {
            return getHumanReadableSize(this.totalSize);
        },
        humanReadableCompletedSize() {
            return getHumanReadableSize(this.completedSize);
        },
        humanReadableInstantSpeed() {
            return getHumanReadableSpeed(this.instantSpeed);
        },
        timeRemaining() {
            return getTimeRemaining(this.totalSize, this.completedSize, this.instantSpeed);
        }
    },
    methods: {
        onDragEnter(event: Event) {
            const element: HTMLElement = <HTMLElement>event.currentTarget;
            element.style.border = "8px solid #a5e159";
        },
        onDragLeave(event: Event) {
            const element: HTMLElement = <HTMLElement>event.currentTarget;
            element.style.border = "0 solid #a5e159";
        },
        onDrop(event: Event) {
            const element: HTMLElement = <HTMLElement>event.currentTarget;
            element.style.border = "0 solid #a5e159";
        },
        copyToClipboard(event: Event) {
            const input: HTMLInputElement = this.$refs.downloadUrl;
            input.select();
            document.execCommand("copy");
        },
        displayFilename(event: Event) {
            const fileInput: HTMLInputElement = <HTMLInputElement>event.currentTarget;

            const file: File = fileInput.files[0];

            this.filename = file.name;
            this.totalSize = file.size;
            this.fileDropped = true;
        },
        upload(event: Event) {
            this.uploading = true;

            const form: HTMLFormElement = <HTMLFormElement>event.currentTarget;

            const file: File = this.$refs.fileInput.files[0];

            const formData = new FormData(form);
            formData.delete("file");
            formData.append("filename", file.name);
            formData.append("size", this.totalSize);

            let dlUrl: string = "";

            axios
                .post("/upload", formData, {withCredentials: true})
                .then(response => {
                    dlUrl = response.data.downloadUrl;
                    return createBlockBlobFromBrowserFile(this, response.data.accountUri, response.data.sasKey, file, response.data.fileId);
                })
                .then(response => {
                    const formData = new FormData();
                    formData.append("payload-id", dlUrl.substr(dlUrl.lastIndexOf("/") + 1));

                    return axios.post("/upload/done", formData, {withCredentials: true});
                })
                .then(response => {
                    this.downloadUrl = dlUrl;
                    this.uploaded = true;
                })
                .catch(error => {
                    console.log(error);
                    this.$refs.errorMessage.showHideError();
                });
        },
        download(event: Event) {
            this.passwordMatch = true;

            const form: HTMLFormElement = <HTMLFormElement>event.currentTarget;
            const formData = new FormData(form);

            axios
                .post("/download", formData)
                .then(response => {
                    location.href = response.data.downloadUrl;
                    this.downloaded = true;
                })
                .catch(error => {
                    if (error.response.status === 401) {
                        this.$refs.passwordInput.passwordMatch = false;
                        setTimeout(() => {
                            this.$refs.passwordInput.passwordMatch = true;
                        }, 3000);
                    } else {
                        console.log(error);
                        this.$refs.errorMessage.showHideError();
                    }
                });
        },
        remove(event: Event) {
            const form: HTMLFormElement = <HTMLFormElement>event.currentTarget;
            const formData: FormData = new FormData(form);

            axios
                .post("/delete", formData, {withCredentials: true})
                .then(response => {
                    const tableData: HTMLTableDataCellElement = <HTMLTableDataCellElement>form.parentNode;
                    const tableRow: HTMLTableRowElement = <HTMLTableRowElement>tableData.parentNode;
                    tableRow.classList.add("slide-horiz-leave-to");
                })
                .catch(error => {
                    console.log(error);
                    this.$refs.errorMessage.showHideError();
                });
        }
    }
});

function getHumanReadableSize(sizeInByte: number): string {
    const kb: number = 1000;
    const mb: number = kb * 1000;
    const gb: number = mb * 1000;

    if (sizeInByte < kb) {
        return `${sizeInByte} B`;
    } else if (sizeInByte < mb) {
        const sizeInKb: number = sizeInByte / kb;
        return `${Number(sizeInKb.toFixed(1))} KB`;
    } else if (sizeInByte < gb) {
        const sizeInMb: number = sizeInByte / mb;
        return `${Number(sizeInMb.toFixed(1))} MB`;
    } else {
        const sizeInGb: number = sizeInByte / gb;
        return `${Number(sizeInGb.toFixed(1))} GB`;
    }
}

function getHumanReadableSpeed(sizeInByte: number): string {
    const kb: number = 1024;
    const mb: number = kb * 1024;
    const gb: number = mb * 1024;

    if (sizeInByte < kb) {
        return `${sizeInByte} B/s`;
    } else if (sizeInByte < mb) {
        const sizeInKb: number = sizeInByte / kb;
        return `${Number(sizeInKb.toFixed(2))} KB/sec`;
    } else if (sizeInByte < gb) {
        const sizeInMb: number = sizeInByte / mb;
        return `${Number(sizeInMb.toFixed(2))} MB/sec`;
    } else {
        const sizeInGb: number = sizeInByte / gb;
        return `${Number(sizeInGb.toFixed(2))} GB/sec`;
    }
}

function getTimeRemaining(totalSize: number, completedSize: number, instantSpeed: number) {
    let time: number = (totalSize - completedSize) / instantSpeed;

    const minute: number = 60;
    const hour: number = minute * 60;
    const day: number = hour * 24;

    if (time < minute) {
        time = Math.ceil(time);
        return time > 1 ? `${time} seconds remaining` : `${time} second remaining`;
    } else if (time < hour) {
        time = Math.ceil(time / minute);
        return time > 1 ? `${time} minutes remaining` : `${time} minute remaining`;
    } else if (time < day) {
        time = Math.ceil(time / hour);
        return time > 1 ? `${time} hours remaining` : `${time} hour remaining`;
    } else {
        time = Math.ceil(time / day);

        if (isFinite(time)) {
            return time > 1 ? `${time} days remaining` : `${time} day remaining`;
        } else {
            return "∞ days remaining";
        }
    }
}

function removeNonAscii(filename: string) {
    let cleanFilename: string = filename.replace(/[^\x00-\x7F]/g, "");
    cleanFilename = cleanFilename.trim();

    if (cleanFilename === "") {
        cleanFilename = "_";
    }

    return cleanFilename;
}

function createBlockBlobFromBrowserFile(vue, accountUri, sasKey, file, fileId) {
    return new Promise((resolve, reject) => {
        const cleanFilename = removeNonAscii(file.name);

        const blobService = AzureStorage.Blob.createBlobServiceWithSas(accountUri, sasKey);

        const customBlockSize = file.size > 32 * 1024 * 1024 ? 4 * 1024 * 1024 : 512 * 1024;
        blobService.singleBlobPutThresholdInBytes = customBlockSize;

        const options = {
            blockSize: customBlockSize,
            contentSettings: {
                contentType: file.type,
                contentDisposition: `attachment; filename="${cleanFilename}"`
            }
        };

        const speedSummary = blobService.createBlockBlobFromBrowserFile("files", fileId, file, options, (error, result, response) => {
            if (error) {
                reject(error);
            } else {
                resolve(result);
            }
        });

        speedSummary.on("progress", () => {
            const completedPercentage = Math.trunc(speedSummary.getCompletePercent());

            vue.$refs.progress.value = completedPercentage;
            vue.completedPercentage = completedPercentage;
            vue.completedSize = speedSummary.getCompleteSize(false);
            vue.instantSpeed = speedSummary.getSpeed(false);
        });
    });
}

const tableRows: HTMLCollection = document.getElementsByTagName("tr");
for (let tableRow of <any>tableRows) {
    tableRow.addEventListener("transitionend", (event: Event) => {
        if (tableRow === event.target) {
            const table: HTMLTableElement = <HTMLTableElement>tableRow.parentNode;
            table.removeChild(tableRow);
        }
    }, false);
}