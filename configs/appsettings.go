package configs

import (
	"encoding/json"
	"fmt"
	"google.golang.org/appengine"
	"google.golang.org/appengine/datastore"
	"log"
	"net/http"
	"os"
)

type applicationSettings struct {
	AzureStorage struct {
		Name string `json:"name"`
		Key  string `json:"key"`
		URI  string `json:"uri"`
	} `json:"azure_storage"`

	Mongo struct {
		ConnectionString string `json:"connection_string"`
		Database         string `json:"database"`
	} `json:"mongo"`
}

var appSettings *applicationSettings

func GetEnvironmentVariables(r *http.Request) *applicationSettings {

	if appSettings == nil {

		appSettings = &applicationSettings{}

		port := os.Getenv("PORT")

		if port == "" {

			file, err := os.Open("appsettings.json")
			if err != nil {
				log.Fatal(err)
			}

			defer file.Close()

			jsonDecoder := json.NewDecoder(file)

			err = jsonDecoder.Decode(&appSettings)
			if err != nil {
				log.Fatal(err)
			}

		} else {

			context := appengine.NewContext(r)

			key := datastore.NewKey(context, "Settings", "env_variables", 0, nil)

			err := datastore.Get(context, key, appSettings)
			if err != nil {
				log.Fatal(err)
			}

		}

		fmt.Printf("Using Azure Storage %s\n", appSettings.AzureStorage.Name)
		fmt.Printf("Using Azure Storage key %s\n", appSettings.AzureStorage.Key)
		fmt.Printf("Using Azure Storage URI %s\n", appSettings.AzureStorage.URI)
		fmt.Printf("Using MongoDB connection string %s\n", appSettings.Mongo.ConnectionString)
		fmt.Printf("Using MongoDB database %s\n", appSettings.Mongo.Database)

	}

	return appSettings

}
