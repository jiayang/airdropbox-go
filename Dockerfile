FROM golang:latest

WORKDIR /go/src/airdropbox

COPY . ./

ENV GO111MODULE on

RUN go mod download
RUN go build -v main.go

ENTRYPOINT ./main
EXPOSE 8080