package internal

import (
	"fmt"
	"strings"
	"time"
)

func GetHumanReadableSize(sizeInByte int) string {

	const kb = 1000.0
	const mb = kb * 1000.0
	const gb = mb * 1000.0

	sizeInB := float64(sizeInByte)

	switch {

	case sizeInB < kb:

		return fmt.Sprintf("%d B", sizeInByte)

	case sizeInB < mb:

		size := sizeInB / kb
		str := fmt.Sprintf("%.1f KB", size)

		return strings.Replace(str, ".0", "", -1)

	case sizeInB < gb:

		size := sizeInB / mb
		str := fmt.Sprintf("%.1f MB", size)

		return strings.Replace(str, ".0", "", -1)

	default:

		size := sizeInB / gb
		str := fmt.Sprintf("%.1f GB", size)

		return strings.Replace(str, ".0", "", -1)

	}
}

func GetRelativeTime(from time.Time, to time.Time) string {

	difference := to.Sub(from)
	differenceInSeconds := int(difference.Seconds())

	const minute = 60
	const hour = minute * 60
	const day = hour * 24

	switch {

	case differenceInSeconds < minute:

		return fmt.Sprintf("%ds", differenceInSeconds)

	case differenceInSeconds < hour:

		quotient := differenceInSeconds / minute
		remainder := differenceInSeconds - (quotient * minute)

		return fmt.Sprintf("%dm %ds", quotient, remainder)

	case differenceInSeconds < day:

		quotient := differenceInSeconds / hour
		remainder := (differenceInSeconds - (quotient * hour)) / minute

		return fmt.Sprintf("%dh %dm", quotient, remainder)

	default:

		quotient := differenceInSeconds / day
		remainder := (differenceInSeconds - (quotient * day)) / hour

		return fmt.Sprintf("%dd %dh", quotient, remainder)

	}
}
