package internal

import (
	"golang.org/x/crypto/bcrypt"
	"log"
)

func HashPassword(password string) string {

	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		log.Fatal(err)
	}

	return string(hash)

}

func Authenticate(hash, password string) bool {

	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))

	if err != nil {

		if err == bcrypt.ErrMismatchedHashAndPassword {

			return false

		}

		log.Fatal(err)
	}

	return true

}
