package handler

import (
	"airdropbox/configs"
	"airdropbox/internal"
	http2 "airdropbox/internal/http"
	"airdropbox/internal/payload"
	"log"
	"net/http"
)

func DeletePOST(w http.ResponseWriter, r *http.Request) {

	data := http2.ReadCookie(r)

	if data == nil {

		w.WriteHeader(http.StatusUnauthorized)

	} else {

		payloadId := r.FormValue("payload-id")

		appSettings := configs.GetEnvironmentVariables(r)

		payloadRepo := payload.NewRepository(appSettings.Mongo.ConnectionString, appSettings.Mongo.Database)
		p := payloadRepo.ReadOneByID(payloadId)

		if p.Owner != data["owner"] {

			w.WriteHeader(http.StatusUnauthorized)

		} else {

			storage := internal.NewCloudStorage(appSettings.AzureStorage.Name, appSettings.AzureStorage.Key)

			_, err := storage.DeleteFile(p.FileID)
			if err != nil {
				log.Fatal(err)
			}

			payloadRepo.DeleteByID(p.PayloadID)

			w.WriteHeader(http.StatusOK)

		}
	}
}