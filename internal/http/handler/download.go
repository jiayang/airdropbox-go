package handler

import (
	"airdropbox/configs"
	"airdropbox/internal"
	"airdropbox/internal/payload"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"html/template"
	"log"
	"net/http"
	"time"
)

type downloadParams struct {
	Title   string
	Payload *payload.Payload
}

func Download(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	payloadId := vars["payloadId"]

	appSettings := configs.GetEnvironmentVariables(r)

	payloadRepo := payload.NewRepository(appSettings.Mongo.ConnectionString, appSettings.Mongo.Database)
	defer payloadRepo.Disconnect()

	p := payloadRepo.ReadOneByID(payloadId)

	if p != nil && p.Downloads < p.MaxDownloads && time.Now().UTC().Before(p.ExpiresAt) {

		params := downloadParams{Title: p.Filename, Payload: p}

		downloadTemplate := template.Must(template.ParseFiles("web/template/download.html", "web/template/layout.html"))

		err := downloadTemplate.ExecuteTemplate(w, "layout", params)
		if err != nil {
			log.Fatal(err)
		}

	} else {

		Error(w, r, http.StatusNotFound)

	}
}

func DownloadPOST(w http.ResponseWriter, r *http.Request) {

	payloadId := r.FormValue("payload-id")
	password := r.FormValue("password")

	appSettings := configs.GetEnvironmentVariables(r)

	payloadRepo := payload.NewRepository(appSettings.Mongo.ConnectionString, appSettings.Mongo.Database)
	defer payloadRepo.Disconnect()

	p := payloadRepo.ReadOneByID(payloadId)

	if p != nil && p.Downloads < p.MaxDownloads && time.Now().UTC().Before(p.ExpiresAt) {

		if p.Password == "" || internal.Authenticate(p.Password, password) {

			storage := internal.NewCloudStorage(appSettings.AzureStorage.Name, appSettings.AzureStorage.Key)
			sasKey := storage.GenerateSignedUrlForDownload(p.FileID)

			downloadUrl := fmt.Sprintf("%s/%s/%s?%s", appSettings.AzureStorage.URI, "files", p.FileID, sasKey)

			payloadRepo.UpdateDownloadsByID(p.PayloadID)

			jsonByte, err := json.Marshal(map[string]string {
				"downloadUrl": downloadUrl,
			})
			if err != nil {
				log.Fatal(err)
			}

			w.WriteHeader(http.StatusOK)

			_, err = w.Write(jsonByte)
			if err != nil {
				log.Fatal(err)
			}

		} else {

			w.WriteHeader(http.StatusUnauthorized)

		}

	} else {

		w.WriteHeader(http.StatusNotFound)

	}
}
