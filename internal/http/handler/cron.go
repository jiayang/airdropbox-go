package handler

import (
	"airdropbox/configs"
	"airdropbox/internal"
	"airdropbox/internal/payload"
	"fmt"
	"github.com/Azure/azure-storage-blob-go/azblob"
	"log"
	"net/http"
)

func Cron(w http.ResponseWriter, r *http.Request) {

	fmt.Println("######## Start of cron job ########")

	appSettings := configs.GetEnvironmentVariables(r)

	payloadRepo := payload.NewRepository(appSettings.Mongo.ConnectionString, appSettings.Mongo.Database)
	defer payloadRepo.Disconnect()

	storage := internal.NewCloudStorage(appSettings.AzureStorage.Name, appSettings.AzureStorage.Key)

	// Delete expired payloads

	expiredPayloads := payloadRepo.ReadExpired()

	fmt.Printf("Found %d expired documents.\n", len(expiredPayloads))

	for i := len(expiredPayloads) - 1; i >= 0; i-- {

		_, err := storage.DeleteFile(expiredPayloads[i].FileID)

		if err != nil && err.ServiceCode() != azblob.ServiceCodeBlobNotFound {

			expiredPayloads = expiredPayloads[0:i]

			fmt.Printf("Error encountered ***. Message:'%s' when deleting a file.\n", err);
			fmt.Printf("Omitted payload: %s\n", *(expiredPayloads[i]))

		}
	}

	if len(expiredPayloads) > 0 {

		ids := []string{}

		for _, p := range expiredPayloads {

			ids = append(ids, p.PayloadID)

		}

		payloadRepo.DeleteByIDs(ids)

	}

	// Delete downloaded payloads

	downloadedPayloads := payloadRepo.ReadDownloaded()

	fmt.Printf("Found %d downloaded documents.\n", len(downloadedPayloads))

	for i := len(downloadedPayloads) - 1; i >= 0; i-- {

		_, err := storage.DeleteFile(downloadedPayloads[i].FileID)

		if err != nil && err.ServiceCode() != azblob.ServiceCodeBlobNotFound {

			downloadedPayloads = downloadedPayloads[0:i]

			fmt.Printf("Error encountered ***. Message:'%s' when deleting a file.\n", err);
			fmt.Printf("Omitted payload: %s\n", *(downloadedPayloads[i]))

		}
	}

	if len(downloadedPayloads) > 0 {

		ids := []string{}

		for _, p := range downloadedPayloads {

			ids = append(ids, p.PayloadID)

		}

		payloadRepo.DeleteByIDs(ids)

	}

	fmt.Println("######## End of cron job ########")

	w.WriteHeader(http.StatusOK)

	_, err := w.Write([]byte("Cron job finished successfully."))
	if err != nil {
		log.Fatal(err)
	}
}
