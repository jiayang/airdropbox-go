package handler

import (
	"airdropbox/configs"
	"airdropbox/internal"
	http2 "airdropbox/internal/http"
	"airdropbox/internal/payload"
	"airdropbox/internal/uid"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"path/filepath"
	"strconv"
	"time"
)

func UploadPOST(w http.ResponseWriter, r *http.Request) {

	data := http2.ReadCookie(r)

	if data == nil {

		w.WriteHeader(http.StatusUnauthorized)

	} else {

		filename := r.FormValue("filename")
		password := r.FormValue("password")

		if password != "" {
			password = internal.HashPassword(password)
		}

		size, err := strconv.Atoi(r.FormValue("size"))
		if err != nil {
			log.Fatal(err)
		}

		maxDownloads, err := strconv.Atoi(r.FormValue("max-downloads"))
		if err != nil {
			log.Fatal(err)
		}

		timeToLive, err := strconv.Atoi(r.FormValue("time-to-live"))
		if err != nil {
			log.Fatal(err)
		}

		payloadId := uid.GenerateFunID()
		fileId := fmt.Sprintf("%s%s", uid.GenerateID(), filepath.Ext(filename))

		appSettings := configs.GetEnvironmentVariables(r)

		storage := internal.NewCloudStorage(appSettings.AzureStorage.Name, appSettings.AzureStorage.Key)

		p := payload.New(
			payloadId,
			fileId,
			filename,
			data["owner"],
			password,
			int(maxDownloads),
			time.Now().UTC().Add(time.Duration(timeToLive)*time.Hour),
			size,
		)

		payloadRepo := payload.NewRepository(appSettings.Mongo.ConnectionString, appSettings.Mongo.Database)
		defer payloadRepo.Disconnect()

		payloadRepo.CreateOne(p)

		jsonByte, err := json.Marshal(map[string]string{
			"accountUri":  appSettings.AzureStorage.URI,
			"sasKey":      storage.GenerateSignedUrlForUpload(fileId),
			"fileId":      fileId,
			"downloadUrl": fmt.Sprintf("%s/%s", r.Host, payloadId),
		})
		if err != nil {
			log.Fatal(err)
		}

		w.WriteHeader(http.StatusOK)

		_, err = w.Write(jsonByte)
		if err != nil {
			log.Fatal(err)
		}
	}
}

func UploadDonePOST(w http.ResponseWriter, r *http.Request) {

	data := http2.ReadCookie(r)

	if data == nil {

		w.WriteHeader(http.StatusUnauthorized)

	} else {

		payloadId := r.FormValue("payload-id")

		appSettings := configs.GetEnvironmentVariables(r)

		payloadRepo := payload.NewRepository(appSettings.Mongo.ConnectionString, appSettings.Mongo.Database)
		defer payloadRepo.Disconnect()

		payloadRepo.UpdateToUploadedByID(payloadId)

		w.WriteHeader(http.StatusOK)

	}
}
