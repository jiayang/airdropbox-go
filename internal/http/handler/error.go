package handler

import (
	"airdropbox/internal"
	"html/template"
	"log"
	"net/http"
)

type errorParams struct {
	Title   string
	Kaomoji string
	Message string
}

func Error(w http.ResponseWriter, r *http.Request, statusCode int) {

	kaomojis := [...]string{"(๑•̌.•̑๑)ˀ̣ˀ̣", "(˃ ⌑ ˂ഃ )", "(；☉_☉)"}

	index := internal.Random.Intn(len(kaomojis))

	params := errorParams{Title: "Oops!", Kaomoji: kaomojis[index]}

	switch statusCode {
	case 404:
		params.Message = "This link has expired or never existed in the first place!"
		break
	default:
		params.Message = "Something broke!"
		break
	}

	errorTemplate := template.Must(template.ParseFiles("web/template/error.html", "web/template/layout.html"))

	w.WriteHeader(statusCode)

	err := errorTemplate.ExecuteTemplate(w, "layout", params)
	if err != nil {
		log.Fatal(err)
	}
}
