package handler

import (
	"airdropbox/configs"
	http2 "airdropbox/internal/http"
	"airdropbox/internal/payload"
	"airdropbox/internal/uid"
	"html/template"
	"log"
	"net/http"
)

type homeParams struct {
	Title        string
	TwoToHundred [99]int
	Payloads     []*payload.Payload
}

func Home(w http.ResponseWriter, r *http.Request) {

	params := homeParams{Title: "AirdropBox"}

	data := http2.ReadCookie(r)

	if data == nil {

		http2.SetCookie(w, map[string]string{"owner": uid.GenerateID()})

	} else {

		appSettings := configs.GetEnvironmentVariables(r)

		payloadRepo := payload.NewRepository(appSettings.Mongo.ConnectionString, appSettings.Mongo.Database)
		defer payloadRepo.Disconnect()

		params.Payloads = payloadRepo.ReadByOwner(data["owner"])

	}

	count := 2

	for i := 0; i < len(params.TwoToHundred); i++ {

		params.TwoToHundred[i] = count
		count++

	}

	indexTemplate := template.Must(template.ParseFiles("web/template/index.html", "web/template/layout.html"))

	err := indexTemplate.ExecuteTemplate(w, "layout", params)
	if err != nil {
		log.Fatal(err)
	}
}
