package http

import (
	"github.com/gorilla/securecookie"
	"log"
	"net/http"
	"time"
)

var hashKey = []byte("very-secret")
var blockKey = []byte("a-lot-lot-secret")
var s = securecookie.New(hashKey, blockKey)

func SetCookie(w http.ResponseWriter, data map[string]string) {

	encoded, err := s.Encode(".airdropbox.Cookie", data)
	if err != nil {
		log.Fatal(err)
	}

	expiration := time.Now().UTC().Add(7 * 24 * time.Hour)

	cookie := http.Cookie{Name: ".airdropbox.Cookie", Value: encoded, Expires: expiration}
	http.SetCookie(w, &cookie)

}

func ReadCookie(r *http.Request) map[string]string {

	data := map[string]string{}

	cookie, err := r.Cookie(".airdropbox.Cookie")

	if err != nil {

		return nil

	}

	err = s.Decode(".airdropbox.Cookie", cookie.Value, &data)
	if err != nil {
		log.Fatal(err)
	}

	return data
}
