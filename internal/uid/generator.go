package uid

import (
	"airdropbox/internal"
	"fmt"
	"github.com/google/uuid"
	"strings"
)

func GenerateFunID() string {
	index := internal.Random.Intn(len(adjectives))
	firstAdj := adjectives[index]

	index = internal.Random.Intn(len(adjectives))
	secondAdj := adjectives[index]

	index = internal.Random.Intn(len(fruitsAndVegetables))
	noun := fruitsAndVegetables[index]

	return fmt.Sprintf("%s%s%s", firstAdj, secondAdj, noun)
}

func GenerateID() string {
	guid := uuid.New()

	guidStr := guid.String()
	guidStr = strings.Replace(guidStr, "-", "", -1)

	return guidStr
}
