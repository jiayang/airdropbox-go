package payload

import (
	"airdropbox/internal"
	"time"
)

type Payload struct {
	PayloadID    string    `bson:"_id"`
	FileID       string    `bson:"fileId,omitempty"`
	Filename     string    `bson:"filename,omitempty"`
	Owner        string    `bson:"owner,omitempty"`
	Password     string    `bson:"password,omitempty"`
	Downloads    int       `bson:"downloads"`
	MaxDownloads int       `bson:"maxDownloads,omitempty"`
	CreatedAt    time.Time `bson:"createdAt,omitempty"`
	ExpiresAt    time.Time `bson:"expiresAt,omitempty"`
	Size         int     `bson:"size,omitempty"`
	Uploaded     bool      `bson:"uploaded"`
}

func New(payloadId, fileId, filename, owner, password string,
	maxDownloads int, expiresAt time.Time, size int) *Payload {

	return &Payload{
		PayloadID:    payloadId,
		FileID:       fileId,
		Filename:     filename,
		Owner:        owner,
		Password:     password,
		Downloads:    0,
		MaxDownloads: maxDownloads,
		CreatedAt:    time.Now().UTC(),
		ExpiresAt:    expiresAt,
		Size:         size,
		Uploaded:     false,
	}
}

func (payload *Payload) RelativeExpiration() string {

	return internal.GetRelativeTime(time.Now().UTC(), payload.ExpiresAt)

}

func (payload *Payload) HumanReadableSize() string {

	return internal.GetHumanReadableSize(payload.Size)

}
