package payload

import (
	"context"
	"fmt"
	"github.com/mongodb/mongo-go-driver/bson"
	"github.com/mongodb/mongo-go-driver/mongo"
	"log"
	"time"
)

type payloadRepository struct {
	client     *mongo.Client
	collection *mongo.Collection
}

func NewRepository(connectionString, database string) *payloadRepository {

	client, err := mongo.Connect(context.TODO(), connectionString)
	if err != nil {
		log.Fatal(err)
	}

	err = client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Connected to MongoDB!")

	collection := client.Database(database).Collection("payloads")

	return &payloadRepository{client: client, collection: collection}

}

func (repo *payloadRepository) Disconnect() {

	err := repo.client.Disconnect(context.TODO())
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Disconnected from MongoDB!")

}

func (repo *payloadRepository) CreateOne(document *Payload) *mongo.InsertOneResult {

	collection := repo.collection

	insertResult, err := collection.InsertOne(context.TODO(), document)
	if err != nil {
		log.Fatal(err)
	}

	return insertResult

}

func (repo *payloadRepository) ReadOneByID(id string) *Payload {

	collection := repo.collection

	filter := bson.M{"_id": id}

	document := &Payload{}

	err := collection.FindOne(context.TODO(), filter).Decode(document)

	if err != nil {

		return nil

	}

	return document

}

func (repo *payloadRepository) ReadByOwner(owner string) []*Payload {

	collection := repo.collection

	filter := bson.M{
		"$and": []bson.M{
			{"owner": owner},
			{"expiresAt": bson.M{"$gt": time.Now().UTC()}},
			{"uploaded": true},
		},
	}

	documents := []*Payload{}

	cur, err := collection.Find(context.TODO(), filter)
	if err != nil {
		log.Fatal(err)
	}

	defer cur.Close(context.TODO())

	for cur.Next(context.TODO()) {

		document := &Payload{}

		err := cur.Decode(document)
		if err != nil {
			log.Fatal(err)
		}

		// Prepend the document.
		documents = append([]*Payload{document}, documents...)

	}

	err = cur.Err()
	if err != nil {
		log.Fatal(err)
	}

	// Reassign the documents in the slice and remove the rest
	count := 0

	for _, document := range documents {

		if document.Downloads < document.MaxDownloads {

			documents[count] = document
			count++

		}
	}

	// Remove the rest
	documents = documents[0:count]

	return documents

}

func (repo *payloadRepository) ReadExpired() []*Payload {

	collection := repo.collection

	filter := bson.M{"expiresAt": bson.M{"$lt": time.Now().UTC()}}

	documents := []*Payload{}

	cur, err := collection.Find(context.TODO(), filter)
	if err != nil {
		log.Fatal(err)
	}

	defer cur.Close(context.TODO())

	for cur.Next(context.TODO()) {

		document := &Payload{}

		err := cur.Decode(document)
		if err != nil {
			log.Fatal(err)
		}

		documents = append(documents, document)

	}

	err = cur.Err()
	if err != nil {
		log.Fatal(err)
	}

	return documents

}

func (repo *payloadRepository) ReadDownloaded() []*Payload {

	collection := repo.collection

	filter := bson.M{"downloads": bson.M{"$gt": 0}}

	documents := []*Payload{}

	cur, err := collection.Find(context.TODO(), filter)
	if err != nil {
		log.Fatal(err)
	}

	defer cur.Close(context.TODO())

	for cur.Next(context.TODO()) {

		document := &Payload{}

		err := cur.Decode(document)
		if err != nil {
			log.Fatal(err)
		}

		documents = append(documents, document)

	}

	err = cur.Err()
	if err != nil {
		log.Fatal(err)
	}

	cleanDocuments := []*Payload{}

	for _, document := range documents {

		if document.Downloads >= document.MaxDownloads {

			cleanDocuments = append(cleanDocuments, document)

		}
	}

	return cleanDocuments

}

func (repo *payloadRepository) UpdateToUploadedByID(id string) *mongo.UpdateResult {

	collection := repo.collection

	filter := bson.M{"_id": id}

	update := bson.M{"$set": bson.M{"uploaded": true}}

	updateResult, err := collection.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		log.Fatal(err)
	}

	return updateResult

}

func (repo *payloadRepository) UpdateDownloadsByID(id string) *mongo.UpdateResult {

	collection := repo.collection

	filter := bson.M{"_id": id}

	update := bson.M{"$inc": bson.M{"downloads": 1}}

	updateResult, err := collection.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		log.Fatal(err)
	}

	return updateResult

}

func (repo *payloadRepository) DeleteByID(id string) *mongo.DeleteResult {

	collection := repo.collection

	filter := bson.M{"_id": id}

	deleteResult, err := collection.DeleteMany(context.TODO(), filter)
	if err != nil {
		log.Fatal(err)
	}

	return deleteResult

}

func (repo *payloadRepository) DeleteByIDs(ids []string) *mongo.DeleteResult {

	collection := repo.collection

	filter := bson.M{"_id": bson.M{"$in": ids}}

	deleteResult, err := collection.DeleteMany(context.TODO(), filter)
	if err != nil {
		log.Fatal(err)
	}

	return deleteResult

}
