package internal

import (
	"context"
	"fmt"
	"github.com/Azure/azure-storage-blob-go/azblob"
	"log"
	"net/url"
	"time"
)

type cloudStorage struct {
	accountName, accountKey string
}

func NewCloudStorage(accountName, accountKey string) *cloudStorage {

	return &cloudStorage{accountName: accountName, accountKey: accountKey}

}

func (storage *cloudStorage) GenerateSignedUrlForUpload(fileId string) string {

	credential, err := azblob.NewSharedKeyCredential(storage.accountName, storage.accountKey)
	if err != nil {
		log.Fatal(err)
	}

	const containerName string = "files"
	blobName := fileId

	sasValues := azblob.BlobSASSignatureValues{
		Protocol:      azblob.SASProtocolHTTPS,
		ExpiryTime:    time.Now().UTC().Add(1 * time.Minute),
		ContainerName: containerName,
		BlobName:      blobName,
		Permissions:   azblob.BlobSASPermissions{Write: true}.String(),
	}

	sasQueryParams, err := sasValues.NewSASQueryParameters(credential)
	if err != nil {
		log.Fatal(err)
	}

	return sasQueryParams.Encode()

}

func (storage *cloudStorage) GenerateSignedUrlForDownload(fileId string) string {

	credential, err := azblob.NewSharedKeyCredential(storage.accountName, storage.accountKey)
	if err != nil {
		log.Fatal(err)
	}

	const containerName string = "files"
	blobName := fileId

	sasValues := azblob.BlobSASSignatureValues{
		Protocol:      azblob.SASProtocolHTTPS,
		ExpiryTime:    time.Now().UTC().Add(1 * time.Minute),
		ContainerName: containerName,
		BlobName:      blobName,
		Permissions:   azblob.BlobSASPermissions{Read: true}.String(),
	}

	sasQueryParams, err := sasValues.NewSASQueryParameters(credential)
	if err != nil {
		log.Fatal(err)
	}

	return sasQueryParams.Encode()

}

func (storage *cloudStorage) DeleteFile(fileId string) (*azblob.BlobDeleteResponse, azblob.StorageError) {

	credential, err := azblob.NewSharedKeyCredential(storage.accountName, storage.accountKey)
	if err != nil {
		log.Fatal(err)
	}

	p := azblob.NewPipeline(credential, azblob.PipelineOptions{})

	const containerName string = "files"
	blobName := fileId

	u, err := url.Parse(fmt.Sprintf("https://%s.blob.core.windows.net/%s", storage.accountName, containerName))
	if err != nil {
		log.Fatal(err)
	}

	containerURL := azblob.NewContainerURL(*u, p)
	blockBlobUrl := containerURL.NewBlockBlobURL(blobName)

	deleteResponse, err := blockBlobUrl.Delete(context.TODO(), azblob.DeleteSnapshotsOptionInclude, azblob.BlobAccessConditions{})

	if err != nil {

		return deleteResponse, err.(azblob.StorageError)

	}

	return deleteResponse, nil
}
