package main

import (
	"airdropbox/internal/http/handler"
	"fmt"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"google.golang.org/appengine"
	"log"
	"net/http"
	"os"
)

func main() {

	router := mux.NewRouter()
	router.PathPrefix("/web/static/").Handler(http.FileServer(http.Dir(".")))

	router.HandleFunc("/", handler.Home).Methods("GET")
	router.HandleFunc("/cron", handler.Cron).Methods("GET")
	router.HandleFunc("/upload", handler.UploadPOST).Methods("POST")
	router.HandleFunc("/upload/done", handler.UploadDonePOST).Methods("POST")
	router.HandleFunc("/download", handler.DownloadPOST).Methods("POST")
	router.HandleFunc("/delete", handler.DeletePOST).Methods("POST")
	router.HandleFunc("/{payloadId}", handler.Download).Methods("GET")

	loggedRouter := handlers.LoggingHandler(os.Stdout, router)

	http.Handle("/", loggedRouter)

	port := os.Getenv("PORT")

	if port == "" {

		port = "8080"
		fmt.Printf("Defaulting to port %s\n", port)
		fmt.Printf("Listening on port %s\n", port)

		log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), loggedRouter))

	} else {

		fmt.Printf("Listening on port %s\n", port)

		appengine.Main()

	}
}