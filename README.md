The purpose of this project is to demonstrate my understanding and knowledge of standard development tools and practices, as well as coding principles.

#### Branches
- [Master](https://gitlab.com/jiayang/airdropbox-go)
- [Vue](https://gitlab.com/jiayang/airdropbox-go/tree/vue-cli)
- [HTML template](https://gitlab.com/jiayang/airdropbox-go/tree/html-template)

##### Master branch
Currently based on [HTML template branch](https://gitlab.com/jiayang/airdropbox-go/tree/html-template)

##### Vue branch
Single page application with Vue.js and RESTful API in Go

##### HTML template branch
Server side rendering with Go html/template package