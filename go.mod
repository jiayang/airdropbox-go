module airdropbox

require (
	github.com/Azure/azure-storage-blob-go v0.0.0-20190104215108-45d0c5e3638e
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/golang/snappy v0.0.0-20180518054509-2e65f85255db // indirect
	github.com/google/uuid v1.1.0
	github.com/gorilla/handlers v1.4.0 // indirect
	github.com/gorilla/mux v1.6.2
	github.com/gorilla/securecookie v1.1.1
	github.com/mongodb/mongo-go-driver v0.2.0
	github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	golang.org/x/crypto v0.0.0-20190103213133-ff983b9c42bc
	golang.org/x/sync v0.0.0-20181221193216-37e7f081c4d4 // indirect
	google.golang.org/appengine v1.4.0
)
