package test

import (
	"airdropbox/internal/uid"
	"testing"
)

func TestGenerateFunID(t *testing.T) {

	outputOne := uid.GenerateFunID()
	outputTwo := uid.GenerateFunID()

	if outputOne == outputTwo {

		t.Fatalf("%s == %s, want different outputs\n", outputOne, outputTwo)

	}
}

func TestGenerateID(t *testing.T) {

	outputOne := uid.GenerateID()
	outputTwo := uid.GenerateID()

	if outputOne == outputTwo {

		t.Fatalf("%s == %s, want different outputs\n", outputOne, outputTwo)

	}
}
