package test

import (
	"airdropbox/internal"
	"log"
	"testing"
	"time"
)

func TestGetHumanReadableSize(t *testing.T) {

	type TestCase struct {
		in     int
		expected string
	}

	testCases := [...]TestCase{
		{in: 1840000000, expected: "1.8 GB"},
		{in: 18400000, expected: "18.4 MB"},
		{in: 184000, expected: "184 KB"},
		{in: 184, expected: "184 B"},
	}

	for _, test := range testCases {

		result := internal.GetHumanReadableSize(test.in)

		if result != test.expected {

			t.Fatalf("%d == %s, want %s\n", test.in, result, test.expected)

		}
	}
}

func TestGetRelativeTime(t *testing.T) {

	type TestCase struct {
		from     string
		to       string
		expected string
	}

	testCases := [...]TestCase{
		{from: "2019-01-01T00:00:00Z", to: "2019-01-01T00:00:10Z", expected: "10s"},
		{from: "2019-01-01T00:00:00Z", to: "2019-01-01T00:10:10Z", expected: "10m 10s"},
		{from: "2019-01-01T00:00:00Z", to: "2019-01-01T10:10:10Z", expected: "10h 10m"},
		{from: "2019-01-01T00:00:00Z", to: "2019-01-02T10:10:10Z", expected: "1d 10h"},
	}

	for _, testCase := range testCases {

		from, err := time.Parse(time.RFC3339, testCase.from)
		if err != nil {
			log.Fatal(err)
		}

		to, err := time.Parse(time.RFC3339, testCase.to)
		if err != nil {
			log.Fatal(err)
		}

		result := internal.GetRelativeTime(from, to)

		if result != testCase.expected {

			t.Fatalf("from %s to %s == %s, want %s\n", from, to, result, testCase.expected)

		}
	}
}
