package test

import (
	"airdropbox/internal"
	"testing"
)

func TestHashPassword(t *testing.T) {

	hashOne := internal.HashPassword("secret password")
	hashTwo := internal.HashPassword("secret password")

	if hashOne == hashTwo {

		t.Fatalf("%s == %s, want different hashes\n", hashOne, hashTwo)

	}
}

func TestAuthenticate(t *testing.T) {

	type TestCase struct {
		password string
		in     string
		expected bool
	}

	testCases := []TestCase{
		{password: "1", in: "1", expected: true},
		{password: "2", in: "2", expected: true},
		{password: "3", in: "3", expected: true},
		{password: "4", in: "123", expected: false},
	}

	for _, testCase := range testCases {

		hash := internal.HashPassword(testCase.password)
		result := internal.Authenticate(hash, testCase.in)

		if result != testCase.expected {

			t.Fatalf("%s and %s == %t, want %t\n", testCase.password, testCase.in, result, testCase.expected)

		}
	}
}
