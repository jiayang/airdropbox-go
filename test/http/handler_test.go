package http

import (
	"airdropbox/internal/http/handler"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHomeHandler(t *testing.T) {

	// Create a request to pass to handler.
	r, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatal(err)
	}

	// Create a ResponseRecorder to record the response.
	resRecorder := httptest.NewRecorder()

	router := mux.NewRouter()
	router.HandleFunc("/", handler.Home)
	router.ServeHTTP(resRecorder, r)

	// Test the status code.
	status := resRecorder.Code

	if status != http.StatusOK {

		t.Errorf("Handler returned wrong status code: got %d, want %d", status, http.StatusOK)

	}
}

func TestUploadHandler(t *testing.T) {

	r, err := http.NewRequest("POST", "/", nil)
	if err != nil {
		t.Fatal(err)
	}

	r.FormValue("filename") = "test filename"
	r.FormValue("password") = "secret password"
	r.FormValue("size") = "1000"
	r.FormValue("max-downloads") = "10"
	r.FormValue("time-to-live") = "12"

	resRecorder := httptest.NewRecorder()

	router := mux.NewRouter()
	router.HandleFunc("/", handler.UploadPOST)
	router.ServeHTTP(resRecorder, r)

	status := resRecorder.Code

	if status != http.StatusOK {

		t.Errorf("Handler returned wrong status code: got %d, want %d", status, http.StatusOK)

	}
}

func TestDownloadHandler(t *testing.T) {

	type TestCase struct {
		payloadId  string
		shouldPass bool
	}

	testCases := [...]TestCase{
		{payloadId: "goroutines", shouldPass: true},
		{payloadId: "heap", shouldPass: true},
		{payloadId: "counters", shouldPass: true},
		{payloadId: "queries", shouldPass: true},
		{payloadId: "adhadaeqm3k", shouldPass: false},
	}

	for _, testCase := range testCases {

		path := fmt.Sprintf("/%s", testCase.payloadId)

		r, err := http.NewRequest("GET", path, nil)
		if err != nil {
			t.Fatal(err)
		}

		resRecorder := httptest.NewRecorder()

		router := mux.NewRouter()
		router.HandleFunc("/{payloadId}", handler.Download)
		router.ServeHTTP(resRecorder, r)

		if resRecorder.Code == http.StatusOK && !testCase.shouldPass {

			t.Errorf("Handler should have failed on %s: got %d, want %d", testCase.payloadId, resRecorder.Code, http.StatusOK)

		}

		if resRecorder.Code != http.StatusOK && testCase.shouldPass {

			t.Errorf("Handler should have passed on %s: got %d, want %d", testCase.payloadId, resRecorder.Code, http.StatusOK)

		}
	}
}

func TestCronHandler(t *testing.T) {

	r, err := http.NewRequest("GET", "/cron", nil)
	if err != nil {
		t.Fatal(err)
	}

	resRecorder := httptest.NewRecorder()

	router := mux.NewRouter()
	router.HandleFunc("/cron", handler.Cron)
	router.ServeHTTP(resRecorder, r)

	status := resRecorder.Code

	if status != http.StatusOK {

		t.Errorf("Handler returned wrong status code: got %d, want %d", status, http.StatusOK)

	}
}
