package repository

import (
	"airdropbox/configs"
	"airdropbox/internal/payload"
	"testing"
	"time"
)

func TestCreateOne(t *testing.T) {

	testCases := [...]*payload.Payload{
		payload.New("id-1", "", "", "owner-1", "", 10, time.Now().UTC().Add(time.Duration(6)*time.Hour), 0),
		payload.New("id-2", "", "", "owner-1", "", 10, time.Now().UTC().Add(time.Duration(6)*time.Hour), 0),
		payload.New("id-3", "", "", "owner-1", "", 10, time.Now().UTC().Add(time.Duration(-1)*time.Hour), 0),
		payload.New("id-4", "", "", "owner-1", "", 10, time.Now().UTC().Add(time.Duration(6)*time.Hour), 0),
		payload.New("id-5", "", "", "owner-2", "", 10, time.Now().UTC().Add(time.Duration(6)*time.Hour), 0),
	}

	testCases[0].Uploaded = true
	testCases[1].Uploaded = true
	testCases[1].Downloads = 10
	testCases[2].Uploaded = true

	appSettings := configs.GetEnvironmentVariables(nil)

	payloadRepo := payload.NewRepository(appSettings.Mongo.ConnectionString, appSettings.Mongo.Database)
	defer payloadRepo.Disconnect()

	for _, testCase := range testCases {

		createResult := payloadRepo.CreateOne(testCase)

		if createResult.InsertedID == nil {

			t.Fatalf("Failed to insert %s", testCase.PayloadID)

		}
	}
}

func TestReadOneByID(t *testing.T) {

	type TestCase struct {
		in       string
		expected *payload.Payload
	}

	testCases := [...]TestCase{
		{in: "id-1", expected: &payload.Payload{}},
		{in: "id-no", expected: nil},
	}

	appSettings := configs.GetEnvironmentVariables(nil)

	payloadRepo := payload.NewRepository(appSettings.Mongo.ConnectionString, appSettings.Mongo.Database)
	defer payloadRepo.Disconnect()

	for _, testCase := range testCases {

		result := payloadRepo.ReadOneByID(testCase.in)

		if (result == nil && testCase.expected != nil) || (result != nil && testCase.expected == nil) {

			t.Fatalf("%s == %v, want %v\n", testCase.in, result, testCase.expected)

		}
	}
}

func TestReadByOwner(t *testing.T) {

	type TestCase struct {
		in       string
		expected int
	}

	testCases := [...]TestCase{
		{in: "owner-1", expected: 1},
		{in: "owner-no", expected: 0},
	}

	appSettings := configs.GetEnvironmentVariables(nil)

	payloadRepo := payload.NewRepository(appSettings.Mongo.ConnectionString, appSettings.Mongo.Database)
	defer payloadRepo.Disconnect()

	for _, testCase := range testCases {

		result := payloadRepo.ReadByOwner(testCase.in)

		if len(result) != testCase.expected {

			t.Fatalf("%s == %d, want %d\n", testCase.in, len(result), testCase.expected)

		}
	}
}

func TestReadExpired(t *testing.T) {

	appSettings := configs.GetEnvironmentVariables(nil)

	payloadRepo := payload.NewRepository(appSettings.Mongo.ConnectionString, appSettings.Mongo.Database)
	defer payloadRepo.Disconnect()

	result := payloadRepo.ReadExpired()
	expected := 1

	if len(result) != expected {

		t.Fatalf("Got %d, want %d\n", len(result), expected)

	}
}

func TestReadDownloaded(t *testing.T) {

	appSettings := configs.GetEnvironmentVariables(nil)

	payloadRepo := payload.NewRepository(appSettings.Mongo.ConnectionString, appSettings.Mongo.Database)
	defer payloadRepo.Disconnect()

	result := payloadRepo.ReadDownloaded()
	expected := 1

	if len(result) != expected {

		t.Fatalf("Got %d, want %d\n", len(result), expected)

	}
}

func TestUpdateToUploadedByID(t *testing.T) {

	type TestCase struct {
		in       string
		expected int
	}

	testCases := [...]TestCase{
		{in: "id-4", expected: 1},
		{in: "id-no", expected: 0},
	}

	appSettings := configs.GetEnvironmentVariables(nil)

	payloadRepo := payload.NewRepository(appSettings.Mongo.ConnectionString, appSettings.Mongo.Database)
	defer payloadRepo.Disconnect()

	for _, testCase := range testCases {

		updateResult := payloadRepo.UpdateToUploadedByID(testCase.in)

		if int(updateResult.ModifiedCount) != testCase.expected {

			t.Fatalf("Got %d, want %d\n", updateResult.ModifiedCount, testCase.expected)

		}
	}
}

func TestUpdateDownloadsByID(t *testing.T) {

	type TestCase struct {
		in       string
		expected int
	}

	testCases := [...]TestCase{
		{in: "id-4", expected: 1},
		{in: "id-no", expected: 0},
	}

	appSettings := configs.GetEnvironmentVariables(nil)

	payloadRepo := payload.NewRepository(appSettings.Mongo.ConnectionString, appSettings.Mongo.Database)
	defer payloadRepo.Disconnect()

	for _, testCase := range testCases {

		updateResult := payloadRepo.UpdateDownloadsByID(testCase.in)

		if int(updateResult.ModifiedCount) != testCase.expected {

			t.Fatalf("Got %d, want %d\n", updateResult.ModifiedCount, testCase.expected)

		}
	}
}

func TestDeleteByID(t *testing.T) {

	type TestCase struct {
		in       string
		expected int
	}

	testCases := [...]TestCase{
		{in: "id-4", expected: 1},
		{in: "id-no", expected: 0},
	}

	appSettings := configs.GetEnvironmentVariables(nil)

	payloadRepo := payload.NewRepository(appSettings.Mongo.ConnectionString, appSettings.Mongo.Database)
	defer payloadRepo.Disconnect()

	for _, testCase := range testCases {

		deleteResult := payloadRepo.DeleteByID(testCase.in)

		if int(deleteResult.DeletedCount) != testCase.expected {

			t.Fatalf("Got %d, want %d\n", deleteResult.DeletedCount, testCase.expected)

		}
	}
}

func TestDeleteByIDs(t *testing.T) {

	testCases := []string{
		"id-1",
		"id-2",
		"id-3",
		"id-4",
		"id-5",
		"id-6",
	}

	appSettings := configs.GetEnvironmentVariables(nil)

	payloadRepo := payload.NewRepository(appSettings.Mongo.ConnectionString, appSettings.Mongo.Database)
	defer payloadRepo.Disconnect()

	deleteResult := payloadRepo.DeleteByIDs(testCases)
	expected := 4

	if int(deleteResult.DeletedCount) != expected {

		t.Fatalf("Got %d, want %d\n", deleteResult.DeletedCount, expected)

	}
}
